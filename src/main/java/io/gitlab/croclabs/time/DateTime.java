package io.gitlab.croclabs.time;

import lombok.Getter;

import java.time.*;
import java.time.temporal.TemporalUnit;
import java.util.function.UnaryOperator;

@Getter
public final class DateTime {
	private Instant instant;
	private ZonedDateTime zonedDateTime;
	private OffsetDateTime offsetDateTime;
	private LocalDateTime localDateTime;
	private ZoneId zoneId;

	public DateTime() {
		this(ZoneId.systemDefault());
	}

	public DateTime(ZoneId zoneId) {
		this(Instant.now(), zoneId);
	}

	public DateTime(Instant instant) {
		this(instant, ZoneId.systemDefault());
	}

	public DateTime(Instant instant, ZoneId zoneId) {
		this.instant = instant;
		this.zoneId = zoneId;
		compute();
	}

	public DateTime plus(long amount, TemporalUnit unit) {
		this.instant = this.instant.plus(amount, unit);
		compute();
		return this;
	}

	public DateTime minus(long amount, TemporalUnit unit) {
		this.instant = this.instant.minus(amount, unit);
		compute();
		return this;
	}

	public DateTime adjust(UnaryOperator<Instant> instantFunction) {
		this.instant = instantFunction.apply(this.instant);
		compute();
		return this;
	}

	public DateTime atZone(ZoneId zoneId) {
		this.zoneId = zoneId;
		compute();
		return this;
	}

	private void compute() {
		this.zonedDateTime = ZonedDateTime.ofInstant(this.instant, this.zoneId);
		this.offsetDateTime = this.zonedDateTime.toOffsetDateTime();
		this.localDateTime = this.zonedDateTime.toLocalDateTime();
	}

	@Override
	public String toString() {
		return """
				UTC:\t\t\t%s
				Zoned:\t\t\t%s
				Offset:\t\t\t%s
				Local:\t\t\t%s
				"""
				.formatted(
						instant.toString(),
						zonedDateTime.toString(),
						offsetDateTime.toString(),
						localDateTime.toString()
				).trim();
	}
}
