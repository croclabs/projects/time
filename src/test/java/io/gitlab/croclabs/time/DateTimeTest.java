package io.gitlab.croclabs.time;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DateTimeTest {

	@Test
	void testToString() {
		String expected = """
				UTC:\t\t\t2024-05-04T12:34:45Z
				Zoned:\t\t\t2024-05-04T08:34:45-04:00[America/New_York]
				Offset:\t\t\t2024-05-04T08:34:45-04:00
				Local:\t\t\t2024-05-04T08:34:45
				""".trim();

		DateTime dt = new DateTime(Instant.parse("2024-05-04T12:34:45Z"));
		String actual = dt.atZone(ZoneId.of("America/New_York")).toString();

		assertEquals(expected, actual);
	}

	@Test
	void testAdjustments() {
		String expected = """
				UTC:\t\t\t2024-05-04T12:34:45Z
				Zoned:\t\t\t2024-05-04T08:34:45-04:00[America/New_York]
				Offset:\t\t\t2024-05-04T08:34:45-04:00
				Local:\t\t\t2024-05-04T08:34:45
				""".trim();

		DateTime dt = new DateTime(Instant.parse("2024-05-04T12:34:45Z"));
		String actual = dt.atZone(ZoneId.of("America/New_York")).toString();

		assertEquals(expected, actual);

		String expected2 = """
				UTC:\t\t\t2024-05-04T14:34:45Z
				Zoned:\t\t\t2024-05-04T10:34:45-04:00[America/New_York]
				Offset:\t\t\t2024-05-04T10:34:45-04:00
				Local:\t\t\t2024-05-04T10:34:45
				""".trim();

		dt.adjust(instant -> instant.plus(2, ChronoUnit.HOURS));
		assertEquals(expected2, dt.toString());

		String expected3 = """
				UTC:\t\t\t2024-05-04T15:34:45Z
				Zoned:\t\t\t2024-05-04T11:34:45-04:00[America/New_York]
				Offset:\t\t\t2024-05-04T11:34:45-04:00
				Local:\t\t\t2024-05-04T11:34:45
				""".trim();

		dt.plus(1, ChronoUnit.HOURS);
		assertEquals(expected3, dt.toString());

		dt.minus(1, ChronoUnit.HOURS);
		assertEquals(expected2, dt.toString());
	}

	@Test
	void testGeneral() {
		DateTime dt = new DateTime();

		assertNotNull(dt);
		assertNotNull(dt.getInstant());
		assertNotNull(dt.getOffsetDateTime());
		assertNotNull(dt.getLocalDateTime());
		assertNotNull(dt.getZonedDateTime());
		assertNotNull(dt.getZoneId());
	}
}